#!/usr/bin/python3
# vim: set cin fileencoding=utf-8 et ts=4 sw=4 tw=80:

import telepot
import time
import os
from os import path
import logging
from datetime import datetime as dt
from wndef import wndef
from anton import antons
from synon import synons
from inputparse import invalid_input
import wnexcept as wnx

TOKEN = '319098927:AAExrobwJCf7dWhOjf52kGBjlgBOCZP8pNg'

bot = telepot.Bot(TOKEN)
global callback_count

PREAMBLE = ('I am a bot that helps you with your English vocabulary by '
             'harnessing the power of the freely available '
             'lexical database of English words '
             '[WordNet™](http://wordnet.princeton.edu) from '
             'Princeton University.\n')

logdir = 'Logs'
cwd    = os.getcwd()
try:
    os.mkdir(path.join(cwd, 'Logs'))
except FileExistsError:
    pass

# For compatibilty with python <= 3.5, don't use timespec keyowrd for isoformat
logfile = (dt.now().isoformat(sep='-').replace(':','-')
           + '.log')
logfile = path.join(cwd, 'Logs', logfile)
logging.basicConfig(filename=logfile,
                    format='[{asctime:15s}] {levelname:8s}: {message:s}',
                    style='{', datefmt='%Y-%m-%d %H:%M:%S %Z',
                    level=logging.INFO)
logging.info('WORDNETBOT LOGGING STARTS...')

def commhelp():
    mess  = PREAMBLE
    mess += '\nThe following commands are currently defined:'
    mess += ('\n'
             '`/wndefine` - Definition of word/phrase.\n'
             '`/wnanton`  - Antonym(s) for word/phrase.\n'
             '`/wnsynon`  - Synonym(s) for word/phrase.\n'
             '`/help`     - Display this message.'
            )
    return(mess)

def startmess():
    mess  = PREAMBLE
    mess += ('\nTry the `\help` command for a list of currently defined '
             'commands.')
    return(mess)

def handle(msg):
    '''This is where the message msg is handled by the telepot api
       and sent to the different functions.
    '''
    content_type, chat_type, chat_id = telepot.glance(msg)
    print(msg)
    logging.info('Message #%d (%s%s) received from id#%d: %s',
                 msg['message_id'],
                 msg['chat']['type'],
                 (':#'+str(msg['chat']['id'])) if msg['chat']['type']=='group' else '',
                 msg['from']['id'],
                 msg['text'])

    try:
        if content_type == 'text':
            msgs = msg['text'].split()
            if ((msgs[0] == '/wndefine') or (msgs[0] == '/wndefine@wordnetdict_bot')):
                if (len(msgs) >= 2):
                    bot.sendMessage(chat_id, wndef(msgs[1:]),
                                    parse_mode='Markdown')

            elif ((msgs[0] == '/wnanton') or (msgs[0] == '/wnanton@wordnetdict_bot')):
                if (len(msgs) >= 2):
                    bot.sendMessage(chat_id, antons(msgs[1:]),
                                    parse_mode='Markdown')

            elif ((msgs[0] == '/wnsynon') or (msgs[0] == '/wnsynon@wordnetdict_bot')):
                if (len(msgs) >= 2):
                    bot.sendMessage(chat_id, synons(msgs[1:]),
                                    parse_mode='Markdown')

            elif ((msgs[0] == '/help') or (msgs[0] == '/help@wordnetdict_bot')):
                bot.sendMessage(chat_id, commhelp(), parse_mode='Markdown')

            elif (msgs[0] == '/start'):
                bot.sendMessage(chat_id, startmess(), parse_mode='Markdown')

            else:
                bot.sendMessage(chat_id, 'No valid command issued.')
    except invalid_input as exc:
        logging.error('Invalid input received: %s', exc.inputstr)
        bot.sendMessage(chat_id, exc.message, parse_mode='Markdown')
    except wnx.WNNotFound as nof:
        logging.warning('No matching entry in WN database for: %s', nof.inputstr)
        bot.sendMessage(chat_id, nof.message, parse_mode='Markdown')
    except wnx.WNNoSyn as nsyn:
        logging.warning('No synonym found for: %s', nsyn.inputstr)
        bot.sendMessage(chat_id, nsyn.message, parse_mode='Markdown')
    except wnx.WNNoAnt as nant:
        logging.warning('No antonym found for: %s', nant.inputstr)
        bot.sendMessage(chat_id, nant.message, parse_mode='Markdown')
    else:
        logging.info('Success!')

bot.message_loop({'chat': handle})
print('Listening ...')
# Keep the program running.
try:
    while 1:
        time.sleep(10)
except KeyboardInterrupt:
    print('Received keyboard interrupt, flushing and closing logfile...')
    logging.shutdown()
    print('All done. Bye!')

