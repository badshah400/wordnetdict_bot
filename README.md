#WordNet lookup bot on telegram#

This is a telegram bot that looks up a specified word on the
[WordNet™](http://wordnet.princeton.edu) Lexical Database
from Princeton University.

This bot makes use of the wonderful [telepot](https://github.com/nickoala/telepot)
and [nltk](http://www.nltk.org/) API's to fetch results for definitions,
synonyms and antonyms.

This code is distributed under the terms of the MIT license (see COPYING)
