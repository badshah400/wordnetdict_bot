#!/usr/bin/python3
# vim: set cin et ts=4 sw=4 tw=80:

import sys
import os
from os import path
import logging
from inputparse import parse, invalid_input
from nltk.corpus import wordnet as wn
import wnexcept as wnx

def antons(lstr):
    '''Returns a comma-separated strings of antonyms, if any
    '''

    try:
        humstr, word = parse(lstr)
    except invalid_input as exc:
        raise
        return (exc.message)

    syns = wn.synsets(word)

    if not len(syns):
        raise wnx.WNNotFound(humstr)

    mess = 'Antonym(s) for *{:s}*: '.format(humstr)

    antwords = []
    for s in syns:
        for l in s.lemmas():
            for w in l.antonyms():
                werd = w.name().replace('_', ' ')
                if werd not in antwords:
                    antwords.append(werd)

    if not len(antwords):
        raise wnx.WNNoAnt(humstr)

    mess += ', '.join(antwords)

    return(mess)

if __name__ == '__main__':
    mess = ''
    if len(sys.argv) > 1:
        try:
            mess = antons(sys.argv[1:])
        except invalid_input as exc:
            mess = exc.message
        except wnx.WNNotFound as nof:
            mess = nof.message
        except wnx.WNNoAnt as noant:
            mess = noant.message

    print(mess)
