#!/usr/bin/python3
# vim: set cin et ts=4 sw=4 tw=80:

import sys
import os
from os import path
import re
from inputparse import parse, invalid_input
from nltk.corpus import wordnet as wn
import wnexcept as wnx

def titlecase(s):
    return re.sub(r"[A-Za-z]+('[A-Za-z]+)?",
                  lambda mo: mo.group(0)[0].upper() +
                             mo.group(0)[1:], s)


def wndef(lstr):
    '''Return the definitions for a lookup match in the WN database
    '''

    try:
        humstr, synstr = parse(lstr)
    except invalid_input as exc:
        raise
        return (exc.message)

    mats = wn.synsets(synstr)

    if not len(mats):
        raise wnx.WNNotFound(humstr)
        return('No match found for {:s}'.format(humstr))

    mess  = '*{:s}*\n'.format(titlecase(humstr))
    if (wn.morphy(synstr) != synstr) and wn.morphy(synstr):
        mess += '_from_ {:s}\n'.format(wn.morphy(synstr))

    sppart = {wn.NOUN : 'noun', wn.ADJ : 'adj', wn.ADV : 'adv', wn.VERB : 'verb'}

    for k in sppart.keys():
        if not len(wn.synsets(synstr, pos=k)):
            continue

        mess += '\n_{:s}_\n'.format(sppart[k])
        for i, syn in enumerate(wn.synsets(synstr, pos=k)):
            defn = '{:s}'.format(syn.definition())
            # The angled quote "`" is used for markdown formatting, if it
            # occurs in the defn (see 'good'), replace it by usual quote
            defn = defn.replace('`', '\'')

            # Upper case the defn first letter, without affecting the rest of
            # the sentence
            mess += '{:d}. {:s}'.format(i+1, defn[0].upper() + defn[1:])

            synlemms = syn.lemma_names()
            idword   = [synstr.lower(), synstr.upper(), synstr.capitalize(),
                        synstr.casefold()]

            for x in idword:
                if x in synlemms:
                    synlemms.remove(x)

            synlemms = [w.replace('_', ' ') for w in synlemms]
            if len(synlemms) > 0:
                mess += '; ' + '; '.join(synlemms)

            mess += ('.\n' if mess[-1] != '.' else '\n')
            if len(syn.examples()) > 0:
                mess += '       _\'{:s}\'_\n'.format(syn.examples()[0])

            alsos  = []
            simtos = []
            for w in syn.also_sees():
                pos  = w.pos()
                werd = '.'.join(w.name().split('.')[:-2])
                werd = werd.replace('_', ' ')
                if werd not in alsos:
                    alsos.append(werd)
            for w in syn.similar_tos():
                pos  = w.pos()
                werd = '.'.join(w.name().split('.')[:-2])
                werd = werd.replace('_', ' ')
                if werd not in simtos:
                    simtos.append(werd)

            if alsos:
                mess += '       _See also:_ ' + ', '.join(alsos) + '.\n'
            if simtos:
                mess += '       _Similar to:_ ' + ', '.join(simtos) + '.\n'

    return(mess)

if __name__ == '__main__':
    mess = 'Error: No input word received.'
    if len(sys.argv) > 1:
        try:
            mess = wndef(sys.argv[1:])
        except invalid_input as exc:
            mess = exc.message
        except wnx.WNNotFound as nof:
            mess = nof.message

    print(mess)
