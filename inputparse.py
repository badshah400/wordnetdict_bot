#!/usr/bin/python3
# vim: set cin et ts=4 sw=4 tw=80:

import sys
import os
import re

class invalid_input(Exception):
    '''Class to raise exceptions when invalid input is detected
    '''

    def __init__(self, inputstr):
        self.inputstr = inputstr
        self.message = '*Error*: Input word/phrase contains invalid characters.'

def parse(lstr):
    '''Parses, sanitises input phrase and returns a tuple of human readable
       and wordnet input phrases
    '''

    failreg = re.compile('[^a-zA-Z0-9\'\.-]')
    for s in lstr:
        if failreg.search(s):
            raise invalid_input(s)

    humstr = ' '.join(lstr)

    print(humstr)
    synstr = '_'.join(lstr)
    return(humstr,synstr)


if __name__ == '__main__':
    mess = 'Error: No input word received.'
    if len(sys.argv) > 1:
        try:
            mess = parse(sys.argv[1:])
        except invalid_input as exc:
            mess = exc.message

    print(mess)
