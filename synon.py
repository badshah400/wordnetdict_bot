#!/usr/bin/python3
# vim: set cin et ts=4 sw=4 tw=80:

import sys
import os
from os import path
from inputparse import parse, invalid_input
from nltk.corpus import wordnet as wn
import wnexcept as wnx

def synons(lstr):
    '''Returns a comma-separated strings of antonyms, if any
    '''

    try:
        humstr, word = parse(lstr)
    except invalid_input as exc:
        raise
        return (exc.message)

    syns = wn.synsets(word)
    if not len(syns):
        raise wnx.WNNotFound(humstr)

    mess = 'Synonym(s) for _{:s}_: '.format(humstr)

    synwords = []
    idwords  = [word.lower(), word.upper(), word.capitalize(),
                word.casefold()]

    for s in syns:
        for l in s.lemma_names():
            werd = l
            if werd == wn.morphy(word):
                continue
            werd =  werd.replace('_', ' ')
            if werd not in synwords and werd not in idwords:
                synwords.append(werd)

    mess += ', '.join(synwords)
    mess += '.'

    if not len(synwords):
        raise wnx.WNNoSyn(humstr)

    return(mess)

if __name__ == '__main__':
    mess = ''
    if len(sys.argv) > 1:
        try:
            mess = synons(sys.argv[1:])
        except invalid_input as exc:
            mess = exc.message
        except wnx.WNNotFound as nof:
            mess = nof.message
        except wnx.WNNoSyn as nosyn:
            mess = nosyn.message

    print(mess)
