# vim: set cin et ts=4 sw=4 tw=80:

class WNNotFound(Exception):
    '''Class to raise exceptions when no match is found in WN for a non-invalid
       input
    '''

    def __init__(self, inputstr):
        self.inputstr = inputstr
        self.message  = '*Error*: No matching entry in WN database for _{:s}_'.format(self.inputstr)

class WNNoSyn(Exception):
    '''Class to raise exceptions when no synonym is found in WN for a
       valid input
    '''

    def __init__(self, inputstr):
        self.inputstr = inputstr
        self.message  = '*Error*: No synonym found for _{:s}_.'.format(self.inputstr)

class WNNoAnt(Exception):
    '''Class to raise exceptions when no antonym is found in WN for a
       valid input
    '''

    def __init__(self, inputstr):
        self.inputstr = inputstr
        self.message  = '*Error*: No antonym found for _{:s}_.'.format(self.inputstr)


